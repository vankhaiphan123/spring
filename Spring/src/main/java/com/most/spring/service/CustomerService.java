package com.most.spring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.most.spring.entity.CustomerVO;

@Service
@Transactional
public interface CustomerService {

	List<CustomerVO> listCustomer(CustomerVO vo);

	void insertCustomer(CustomerVO vo);

	CustomerVO getCustomer(CustomerVO vo);

	void updateCustomer(CustomerVO vo);

	void deleteCustomer(CustomerVO vo);

	List<CustomerVO> listSearchCustomer(String keyword);

}
