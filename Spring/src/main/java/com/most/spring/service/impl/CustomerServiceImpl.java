package com.most.spring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.most.spring.entity.CustomerVO;
import com.most.spring.repository.CustomerRepository;
import com.most.spring.service.CustomerService;

@Service("CustomerServiceImpl")
public class CustomerServiceImpl implements CustomerService {
	
	private CustomerRepository cusRepo;
	
	@Autowired
	public void CustomerService(CustomerRepository cusRepo) {
		this.cusRepo = cusRepo;
	}
	
	@Override
	public void insertCustomer(CustomerVO vo) {
		cusRepo.insert(vo);
	}

	@Override
	public List<CustomerVO> listCustomer(CustomerVO vo) {
		return cusRepo.list(vo);
	}
	
	@Override
	public CustomerVO getCustomer(CustomerVO vo) {
		return cusRepo.get(vo);
	}

	@Override
	public void updateCustomer(CustomerVO vo) {
		cusRepo.update(vo);
	}

	@Override
	public void deleteCustomer(CustomerVO vo) {
		cusRepo.delete(vo);
	}

	@Override
	public List<CustomerVO> listSearchCustomer(String keyword) {
		return cusRepo.listSearch(keyword);
	}
}
