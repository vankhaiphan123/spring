package com.most.spring.repository;

import java.util.List;

import com.most.spring.entity.CustomerVO;

public interface CustomerRepository extends DAO<CustomerVO>{

	List<CustomerVO> listSearch(String keyword);

}
