package com.most.spring.repository;

import java.util.List;

public interface DAO<T> {

	// CRUD methods
	void insert(T vo);

	void delete(T vo);

	void update(T vo);

	List<T> list(T vo);

	T get(T vo);

}