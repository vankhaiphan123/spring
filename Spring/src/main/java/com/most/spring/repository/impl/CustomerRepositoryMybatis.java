package com.most.spring.repository.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.most.spring.entity.CustomerVO;
import com.most.spring.repository.CustomerRepository;

// repository is DAO
@Repository 
public class CustomerRepositoryMybatis implements CustomerRepository {
	
	@Autowired
	private SqlSessionTemplate session;
	
	@Override
	public void insert(CustomerVO vo) {
		session.insert("Customer.insert", vo);
	}

	@Override
	public void delete(CustomerVO vo) {
		session.delete("Customer.delete", vo);
	}

	@Override
	public void update(CustomerVO vo) {
		session.update("Customer.update", vo);
	}

	@Override
	public List<CustomerVO> list(CustomerVO vo) {
		return session.selectList("Customer.selectList", vo);
	}

	@Override
	public CustomerVO get(CustomerVO vo) {
		return (CustomerVO)session.selectOne("Customer.selectOne", vo);
	}

	public List<CustomerVO> listSearch(String keyword) {
		return session.selectList("Customer.search", keyword);
	}
	
}
