package com.most.spring.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.most.spring.entity.CustomerVO;
import com.most.spring.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController {
	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);
	
	private CustomerService customerService;
	
	@Autowired
	public CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	@RequestMapping(value = {"","/"}, method = RequestMethod.GET)
	public String listCustomer(Locale locale, Model model) {
		logger.info("List Customer {}.", locale);
		
		List<CustomerVO> list = customerService.listCustomer(null);
		model.addAttribute("listCustomer", list);
		
		return "customer/customer";	
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String newCustomer(Locale locale, Model model) {
		logger.info("Customer New {}.", locale);
		
		return "customer/new_customer";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveCustomer(CustomerVO vo , Locale locale) {
		logger.info("Customer Save {}.", locale);	
		
		customerService.insertCustomer(vo);		
		
		return "redirect:/customer/";		
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String editCUSTOMER(@RequestParam(value = "id") Long id, Locale locale, Model model) {
		logger.info("Customer edit {}.", locale);
		
		CustomerVO vo = new CustomerVO();	
		vo.setId(id.longValue());
		CustomerVO customer = customerService.getCustomer(vo);
		model.addAttribute("customer", customer);		
		
		return "customer/edit_customer";
	}

	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateCustomer(Locale locale, HttpServletRequest rq) {
		logger.info("Customer update {}.", locale);
		
		CustomerVO vo = new CustomerVO();
		vo.setName(rq.getParameter("name"));
		vo.setEmail(rq.getParameter("email"));
		vo.setAddress(rq.getParameter("address"));
		vo.setId(Long.parseLong(rq.getParameter("id")));	
		customerService.updateCustomer(vo);	
		
		return "redirect:/customer";
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String deleteCustomer(@RequestParam(value="id") Long id, Locale locale, Model model) {
		logger.info("Customer Delete {}.", locale);
		
		CustomerVO vo = new CustomerVO();
		vo.setId(id.longValue());
		customerService.deleteCustomer(vo);
			
		return "redirect:/customer";
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String searchCustomer(@RequestParam(value="keyword") String keyword, Locale locale, Model model) {
		logger.info("Customer Search {}.", locale);
		
		System.out.println("keyword = " + keyword);
	
		List<CustomerVO> result = customerService.listSearchCustomer(keyword);
		model.addAttribute("result", result);		
			
		return "customer/search_result";
	}
}




















