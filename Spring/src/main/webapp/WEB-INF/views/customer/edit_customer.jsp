<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>      
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Customer</title>
		<link rel="stylesheet" crossorigin="anonymous"
			href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
			integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" />
</head>
<body>
    <div class="container" align="center">
        <h2>Edit Customer</h2>
        <form:form action="update" method="post" modelAttribute="customer">
            <table class="table table-bordered table-striped">
                <tr>
                    <td>ID </td>
                    <td>${customer.id}
                        <form:hidden path="id"/>
                    </td>
                </tr>        
                <tr>
                    <td  class="form-label" >Name </td>
                    <td><form:input class="form-control" path="name" /></td>
                </tr>
                <tr>
                    <td  class="form-label" >Email </td>
                    <td><form:input  class="form-control" path="email" /></td>
                </tr>
                <tr>
                    <td  class="form-label" >Address </td>
                    <td><form:input  class="form-control" path="address" /></td>
                </tr>    
                <tr>
                    <td colspan="2"><input type="submit" value="Save" class="btn btn-success"></td>
                </tr>                    
            </table>
        </form:form>
    </div>
</body>
</html>
