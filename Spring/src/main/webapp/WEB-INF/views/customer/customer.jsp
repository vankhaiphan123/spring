<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ page import="com.most.spring.entity.CustomerVO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Customer Manager</title>
		<link rel="stylesheet" crossorigin="anonymous"
			href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
			integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" />
	</head>
	<body>
		<div class="container" align="center">
		    <h2>Customer Manager</h2>
		    <form action="customer/search" method="get" >
		        <input type="text" name="keyword" placeholder="Search..." aria-label="Search" class="form-control"/> &nbsp;
		    	<!--  <input type="submit" value="Search" />-->
		    </form>
		    <h3><a href="${pageContext.request.contextPath}/customer/new"  class="btn btn-primary btn-sm">New Customer</a></h3>
		    <table class="table table-bordered table-striped">
		        <thead>
			        <tr>
			            <th>ID</th>
			            <th>Name</th>
			            <th>E-mail</th>
			            <th>Address</th>
			            <th>Action</th>
			        </tr>
		        </thead>
		        <tbody>
			        <c:forEach items="${listCustomer}" var="customer">
				        <tr>
				            <td>${customer.id}</td>
				            <td>${customer.name}</td>
				            <td>${customer.email}</td>
				            <td>${customer.address}</td>
				            <td>
				                <a 	href="${pageContext.request.contextPath}/customer/edit?id=${customer.id}" 
				                	class="btn btn-info">Edit</a>&nbsp;&nbsp;&nbsp;
				                <a 	href="${pageContext.request.contextPath}/customer/delete?id=${customer.id}"
				                	class="btn btn-danger ml-2">Delete</a>
				            </td>
				        </tr>
			        </c:forEach>
		        </tbody>
		    </table>
		</div>
	</body>
</html>